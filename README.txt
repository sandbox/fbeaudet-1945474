jQuery.mosaicflow Drupal Module

About

Pinterest like responsive image grid that doesn’t sucks.
This module adds a new Display format in Views that creates a 
jQuery.Mosaicflow image grid.

How to

Install
1) Download and activate the module.
2) Go in the module directory and do :
git clone git://github.com/sapegin/jquery.mosaicflow.git jquery.mosaicflow
(Or, if you dont have SSH access, download the lib and 
put it in jquery.mosaicflow folder)

Create a view (page/block)
3) Create a new view and choose jQuery.Mosaicflow display format using fields.
4) Field #1: Add an image field that will serve as background images.
5) Field #2: Add a textual field that will serve as descriptions/titles.
6) Save. Customise.
